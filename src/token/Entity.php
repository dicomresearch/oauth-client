<?php

namespace dicom\oauthClient\token;
use dicom\oauthClient\OauthClientException;

/**
 * TokenEntity
 *
 * Сущность токен
 */
class Entity
{
    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var int
     */
    private $expiresIn;

    /**
     * @var string
     */
    private $tokenType;

    /**
     * @var string
     */
    private $scope;

    /**
     * @var string
     */
    private $refreshToken;

    /**
     * @param array $data {
            'access_token' =>
            string(40) "ed214784df547551321babc6f64f7f61cacbcefa"
            'expires_in' =>
            int(3600)
            'token_type' =>
            string(6) "Bearer"
            'scope' =>
            string(9) "mainScope"
            'refresh_token' =>
            string(40) "36398d579c30dd6650fb7fd676c4d17abae3f8d4"
        }
     */
    public function __construct($data)
    {
        $this->validateInitialData($data);

        $this->setAccessToken($data['access_token']);
        $this->setExpiresIn($data['expires_in']);
        $this->setTokenType($data['token_type']);
        $this->setScope($data['scope']);
        $this->setRefreshToken($data['refresh_token']);
    }

    /**
     * Validate init data
     *
     * @param array $data data
     * @throws OauthClientException
     */
    private function validateInitialData($data)
    {
        if (!isset($data['access_token'])) {
            throw OauthClientException::tokenValidationException('access_token');
        }

        if (!isset($data['expires_in'])) {
            throw OauthClientException::tokenValidationException('expires_in');
        }

        if (!isset($data['token_type'])) {
            throw OauthClientException::tokenValidationException('token_type');
        }

        if (!isset($data['scope'])) {
            throw OauthClientException::tokenValidationException('scope');
        }

        if (!isset($data['refresh_token'])) {
            throw OauthClientException::tokenValidationException('refresh_token');
        }
    }

    /**
     * Convert token entity object to array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'access_token' => $this->getAccessToken(),
            'expires_in' => $this->getExpiresIn(),
            'token_type' => $this->getTokenType(),
            'scope' => $this->getScope(),
            'refresh_token' => $this->getRefreshToken()
        ];
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return int
     */
    public function getExpiresIn()
    {
        return $this->expiresIn;
    }

    /**
     * @param int $expiresIn
     */
    public function setExpiresIn($expiresIn)
    {
        $this->expiresIn = $expiresIn;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return string
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
    }

    /**
     * @return string
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * @param string $tokenType
     */
    public function setTokenType($tokenType)
    {
        $this->tokenType = $tokenType;
    }
} 