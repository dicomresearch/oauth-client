<?php

namespace dicom\oauthClient;
use dicom\oauthClient\token\Entity as Token;
use dicom\oauthClient\user\Entity as User;

/**
 * OauthClientProvider
 *
 * Провайдер который инкапсулирует работу с oauth server
 */
class OauthClientProvider
{
    /**
     * @var Configer
     */
    private $configer;

    /**
     * @param Configer $configer
     */
    function __construct($configer)
    {
        $this->configer = $configer;
    }

    /**
     * Отдать сформированный специальным образом путь для запроса authKey
     * Oauth server сам будет разбираться залогинен пользователь в системе или нет
     */
    public function getAuthKeyPath()
    {
        $path = $this->configer->getAuthKeyPath();
        $path .= '?';
        $path .= 'client_id=' . $this->configer->getClientId() . '&';
        $path .= 'response_type=' . $this->configer->getResponseType() . '&';
        $path .= 'scope=' . $this->configer->getScope() . '&';
        $path .= 'state=' . $this->configer->getState() . '&';
        $path .= 'redirect_uri=' . $this->configer->getRedirectUri();

        return $path;
    }

    /**
     * Получить токен по authkey
     *
     * @param $authKey
     */
    public function getToken($authKey)
    {
        if (!$authKey) {
            throw OauthClientException::noAuthKeyProvided();
        }

        $postFields = [
            'grant_type' => $this->configer->getGrantType(),
            'client_id' => $this->configer->getClientId(),
            'client_secret' => $this->configer->getClientSecret(),
            'code' => $authKey,
            'redirect_uri' => $this->configer->getRedirectUri()
        ];
        $result = json_decode($this->makeRequestToOauthServer($this->configer->getTokenPath(), $postFields), true);
        $this->checkResult($result);
        return new Token($result);
    }

    /**
     * Верифицировать что токен не истек, а заодно получить login пользователя
     *
     * @param string $token
     * @return mixed
     */
    public function verifyToken(Token $token = null)
    {
        if (is_null($token)) {
            throw OauthClientException::noTokenProvided();
        }

        $postFields = 'access_token=' . $token->getAccessToken();
        $result = json_decode($this->makeRequestToOauthServer($this->configer->getTokenVerificationPath(), $postFields), true);
        $this->checkResult($result);
        return $result;
    }

    /**
     * Если произошла ошибка выкинем эксепшн
     *
     * @param $result
     * @throws OauthClientException
     */
    private function checkResult($result)
    {
        if (isset($result['error'])) {
            throw OauthClientException::oauthServerResponseException($result['error'], $result['error_description']);
        }

        if (empty($result)) {
            throw OauthClientException::emptyResult();
        }
    }

    /**
     * Получить данные о пользователе с сервера oauth
     *
     * @param $token
     * @return mixed
     */
    public function getUserInfo(Token $token)
    {
        return User::create($this->verifyToken($token)['user']);
    }

    /**
     * Вспомогательный метод, инкапсулирует инициализацию курла
     *
     * @param $path
     * @param array $fields
     * @return resource
     */
    private function makeRequestToOauthServer($path, $fields = null)
    {
        $ch = curl_init($path);

        if (!is_null($fields)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        if (0 !== curl_errno($ch)) {
          throw OauthClientException::oauthServerErrorResponse(curl_errno($ch), curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    /**
     * Обновить токен, который истек, испльзуя refresh token
     * url - https://api.mysite.com/token -d 'grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA'
     *
     * @param Token $token
     * @return mixed
     * @throws OauthClientException
     */
    public function refreshToken(Token $token)
    {
        $postFields = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $token->getRefreshToken(),
            'client_id' => $this->configer->getClientId(),
            'client_secret' => $this->configer->getClientSecret()
        ];
        $refreshedTokenRawData = json_decode($this->makeRequestToOauthServer($this->configer->getTokenPath(), $postFields), true);
        $this->checkResult($refreshedTokenRawData);
        return new Token($refreshedTokenRawData);
    }
}