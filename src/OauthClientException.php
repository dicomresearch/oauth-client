<?php

namespace dicom\oauthClient;

/**
 * OauthClientException 
 */
class OauthClientException extends \Exception
{
    public static function oauthServerResponseException($error, $errorDescription)
    {
        $message = 'Oauth server error: ' . $error . '. ' . $errorDescription;
        return new static($message);
    }

    public static function emptyResult()
    {
        return new static("Oauth server returned empty result");
    }

    public static function noTokenProvided()
    {
        $msg = self::errorPrefix() . "Please provide token for this action!";
        return new static($msg);
    }

    public static function noAuthKeyProvided()
    {
        $msg = self::errorPrefix() . "Please provide auth key for this action!";
        return new static($msg);
    }

    public static function oauthUserDataIdMissing()
    {
        $msg = self::errorPrefix() . "Cant fetch oauth user id from service response";
        return new static($msg);
    }

    public static function oauthUserDataUsernameMissing()
    {
        $msg = self::errorPrefix() . "Cant fetch oauth user username from service response";
        return new static($msg);
    }

    public static function tokenValidationException($param)
    {
        $msg = self::errorPrefix() . $param . " not provided to construct TokenEntity object";
        return new static($msg);
    }

    public static function userIdNotSet()
    {
        $msg = self::errorPrefix() . ' please provide user id!';
        return new static($msg);
    }

    public static function userNotFoundInServerResponse()
    {
        $msg = self::errorPrefix() . ' user not found in server response';
        return new static($msg);
    }

    public static function usersNotFoundInServerResponse()
    {
        $msg = self::errorPrefix() . ' users list not found in server response';
        return new static($msg);
    }

    public static function invalidTransportResult()
    {
        $msg = self::errorPrefix() . ' curl (transport) responded with error';
        return new static($msg);
    }

    public static function invalidServerResponseFormat()
    {
        $msg = self::errorPrefix() . ' server responded with invalid format. Expected JSON';
        return new static($msg);
    }

    private static function errorPrefix()
    {
        return "Oauth client error: ";
    }

    public static function oauthServerErrorResponse($errorCode, $errorMessage)
    {
        return new static(static::errorPrefix() . sprintf(' Oauth server returned: error code: %s, error message: %s', $errorCode, $errorMessage));
    }

    public static function oauthUserDataParameterMissing($parameter)
    {
        $msg = self::errorPrefix() . "Cant fetch oauth user '" . $parameter . "' from service response";
        return new static($msg);
    }

    public static function totalNotFoundInServerResponse()
    {
        $msg = self::errorPrefix() . ' total count of users not found in server response';
        return new static($msg);
    }
}