<?php

namespace dicom\oauthClient;
use dicom\oauthClient\user\Entity as UserEntity;

/**
 * OauthUserProvider
 *
 * Класс обертка над REST API пользователей oauth
 */
class OauthUserProvider 
{
    /**
     * @var Configer
     */
    private $configer;

    /**
     * init
     */
    public function __construct($configer)
    {
        $this->configer = $configer;
    }

    /**
     * Получить пользователя по id
     *
     * @throws OauthClientException
     */
    public function get($id = null)
    {
        if (is_null($id)) {
            throw OauthClientException::userIdNotSet();
        }

        $url = $this->configer->getServerPath() . $this->configer->getUsersApiGetPath() . '?id=' . $id;
        $response = $this->makeRequestToOauthServer($url);

        if (!isset($response['user'])) {
            throw OauthClientException::userNotFoundInServerResponse();
        }

        return UserEntity::create($response['user']);
    }

    /**
     * Найти пользовтелей по параметрам
     *
     * @throws OauthClientException
     * @return array
     */
    public function find($criteria = [], $orderBy = null, $offset = null, $limit = null)
    {
        $url = $this->configer->getServerPath()
            . $this->configer->getUsersApiFindPath()
            . '?criteria='
            . json_encode($criteria);

        if (!is_null($orderBy)) {
            $url .= '&orderBy=' . json_encode($orderBy);
        }

        if (!is_null($limit)) {
            $url .= '&limit=' . $limit;
        }

        if (!is_null($offset)) {
            $url .= '&offset=' . $offset;
        }

        $response = $this->makeRequestToOauthServer($url);

        if (!isset($response['users'])) {
            throw OauthClientException::usersNotFoundInServerResponse();
        }

        if (!isset($response['total'])) {
            throw OauthClientException::usersNotFoundInServerResponse();
        }

        $total = $response['total'];

        $users = [];

        foreach ($response['users'] as $userRawData) {
            $users[$userRawData['id']] = UserEntity::create($userRawData);
        }

        $result = ['total' => $total, 'users' => $users];

        return $result;
    }
    /**
     * Вспомогательный метод, инкапсулирует инициализацию курла
     *
     * @param $path
     * @param array $fields
     * @return resource
     */
    private function makeRequestToOauthServer($path, $fields = null)
    {
        $ch = curl_init($path);

        if (!is_null($fields)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        $this->checkCurlResponse($response);

        curl_close($ch);

        return json_decode($response, true);
    }

    /**
     * Проверить что response от сервера проходит первичную валидацию
     *
     * @param $response
     * @throws OauthClientException
     */
    private function checkCurlResponse($response)
    {
        if (false === $response) {
            throw OauthClientException::invalidTransportResult();
        }

        if (null === json_decode($response, true)) {
            throw OauthClientException::invalidServerResponseFormat();
        }
    }

    /**
     * Сохраняем пользователя на SSO
     *
     * @param array $data
     * @return array
     */
    public function create(array $data)
    {
        $url = $this->configer->getServerPath() . $this->configer->getUsersApiCreatePath();
        $response = $this->makeRequestToOauthServer($url, ['data' => json_encode($data)]);

        return $response;
    }

    /**
     * @param array $data
     * @return array
     */
    public function update(array $data)
    {
        $url = $this->configer->getServerPath() . $this->configer->getUsersApiUpdatePath();
        $response = $this->makeRequestToOauthServer($url, ['data' => json_encode($data)]);

        return $response;
    }

    /**
     * в качестве параметра принимает массив вида
     * array(
     *      'newPassword' => 'newPassword',
     *      'oldPassword' => 'oldPassword',
     *      'userId' => 'userId'
     * );
     *
     * @param array $data
     * @return array
     */
    public function changePassword(array $data)
    {
        $url = $this->configer->getServerPath() . $this->configer->getUsersApiChangePasswordPath();
        $response = $this->makeRequestToOauthServer($url, ['data' => json_encode($data)]);

        return $response;
    }

    /**
     * удаление пользователей на sso
     * принимает в качестве параметра идентификатор
     * пользователя на sso
     *
     * @param $id
     * @return resource
     */
    public function delete($id)
    {
        $url = $this->configer->getServerPath() . $this->configer->getUsersApiDeletePath();
        $response = $this->makeRequestToOauthServer($url, ['id' => $id]);

        return $response;
    }
} 