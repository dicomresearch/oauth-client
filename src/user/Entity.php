<?php

namespace dicom\oauthClient\user;
use dicom\oauthClient\OauthClientException;

/**
 * OauthUserInfo
 *
 * Обертка над данными oauth пользователя
 */
class Entity 
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $middleName;

    /**
     * @var date
     */
    private $birthDate;

    /**
     * Может инстанцироваться пустым массивом
     */
    public function __construct($oauthUserRawData=[])
    {
        if (!empty($oauthUserRawData)) {
            $this->validate($oauthUserRawData);
            $this->username     = $oauthUserRawData['username'];
            $this->id           = $oauthUserRawData['id'];
            $this->firstName    = $oauthUserRawData['firstname'];
            $this->lastName     = $oauthUserRawData['lastname'];
            $this->middleName   = $oauthUserRawData['middlename'];
            $this->birthDate    = $oauthUserRawData['birthdate'];
            $this->email        = $oauthUserRawData['email'];
        }
    }

    /**
     * Фабричный метод
     *
     * @param $oauthUserRawData
     * @return static
     */
    public static function create($oauthUserRawData)
    {
        return new static($oauthUserRawData);
    }

    /**
     * Id с oauth сервиса
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Этот логин  по идее идентичен в телерадиологии и в oauth service
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this->username;
    }

    /**
     * Get Password
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this->password;
    }

    /**
     * Get FirstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this->firstName;
    }

    /**
     * Get LastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return  $this->lastName;
    }

    /**
     * Get MiddleName
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param $middleName
     * @return $this
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
        return $this->middleName;
    }

    /**
     * Get Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this->email;
    }

    /**
     * Get BirthData
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param $birthDate
     * @return $this
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
        return $this->birthDate;
    }

    /**
     * validation of raw data
     *
     * @param $oauthUserRawData
     *
     * @throws OauthClientException
     */
    private function validate($oauthUserRawData)
    {
        if (!isset($oauthUserRawData['id'])) {
            throw OauthClientException::oauthUserDataIdMissing();
        }
        if (!isset($oauthUserRawData['username'])) {
            throw OauthClientException::oauthUserDataUsernameMissing();
        }
        if (!array_key_exists('firstname', $oauthUserRawData)) {
            throw OauthClientException::oauthUserDataParameterMissing('firstname');
        }
        if (!array_key_exists('lastname', $oauthUserRawData)) {
            throw OauthClientException::oauthUserDataParameterMissing('lastname');
        }
        if (!array_key_exists('middlename', $oauthUserRawData)) {
            throw OauthClientException::oauthUserDataParameterMissing('middlename');
        }
        if (!array_key_exists('email', $oauthUserRawData)) {
            throw OauthClientException::oauthUserDataParameterMissing('email');
        }
    }

    /**
     * Конвертировать в массив
     */
    public function toArray()
    {
        return [
            'id'            => $this->getId(),
            'username'      => $this->getUsername(),
            'firstname'     => $this->getFirstName(),
            'lastname'      => $this->getLastName(),
            'middlename'    => $this->getMiddleName(),
            'birthdate'     => $this->getBirthDate(),
            'email'         => $this->getEmail()
        ];
    }
}